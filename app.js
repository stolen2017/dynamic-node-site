var router = require("./router.js");
//problem:we need a simple way to look at a user's badge count and Javascript points from a web browser

//solution:use node.js to perform the profile look ups and server our template via HTTP

//1. create a web server
var http = require('http');
http.createServer(function (request, response) {
    router.home(request, response);
    router.user(request, response);
    //response.writeHead(200, {'Content-Type': 'text/plain'});
    //setInterval(function(){
       // response.write(new Date() + "\n");    
    //}, 1000);

    //response.end('Hello World\n');
}).listen(process.env.PORT, process.env.IP);
console.log("Server running at http://<workspace-url>/");




        



